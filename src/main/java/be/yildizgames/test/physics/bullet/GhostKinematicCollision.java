/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 * Copyright (c) 2018 Grégory Van den Borre
 *
 * More infos available: https://www.yildiz-games.be
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE  SOFTWARE.
 */

package be.yildizgames.test.physics.bullet;

import be.yildizgames.common.geometry.Point3D;
import be.yildizgames.common.shape.Box;
import be.yildizgames.module.physics.GhostObject;
import be.yildizgames.module.physics.KinematicBody;
import be.yildizgames.module.physics.PhysicEngine;
import be.yildizgames.module.physics.PhysicWorld;


class GhostKinematicCollision {

    static void execute(PhysicEngine engine, PhysicWorld world) throws InterruptedException {
        world.addGhostCollisionListener(objects -> System.out.println("ghost:" + objects.object1 + ", " + objects.object2));
        world.createObject()
                .atPosition(Point3D.valueOf(10))
                .withId(8L)
                .withShape(Box.cube(5))
                .buildKinematic();
        KinematicBody body2 = world.createObject()
                .atPosition(Point3D.valueOf(500))
                .withShape(Box.cube(5))
                .withId(9L)
                .buildKinematic();
        GhostObject g = world.createObject()
                .withId(100L)
                .withShape(Box.cube(5))
                .atPosition(Point3D.valueOf(100))
                .buildGhost();
        engine.update();
        Thread.sleep(50);
        engine.update();
        g.setPosition(11, 11, 11);
        engine.update();
        Thread.sleep(50);
        engine.update();
    }
}
