/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 * Copyright (c) 2018 Grégory Van den Borre
 *
 * More infos available: https://www.yildiz-games.be
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE  SOFTWARE.
 */

package be.yildizgames.test.physics.bullet;

import be.yildizgames.common.geometry.Point3D;
import be.yildizgames.common.shape.Sphere;
import be.yildizgames.module.physics.DynamicBody;
import be.yildizgames.module.physics.Gravity;
import be.yildizgames.module.physics.PhysicEngine;
import be.yildizgames.module.physics.PhysicWorld;

class FallWithGravity {

    static void execute(PhysicEngine engine, PhysicWorld world) {
        world.setGravity(Gravity.SPACE);
        DynamicBody body = world.createObject()
                .withMass(5)
                .atPosition(Point3D.valueOfY(17))
                .withId(17)
                .withShape(Sphere.fromRadius(2))
                .buildDynamic();
        while(true) {
            engine.update();
            System.out.println(body.getPosition());
        }

    }
}
